import 'dart:io';

import 'package:flutter/material.dart';

// ignore: must_be_immutable
class AttachmentImageFullView extends StatefulWidget {
  String imageUrl, filetype;

  final _transformationController = TransformationController();
  TapDownDetails _doubleTapDetails;
  AttachmentImageFullView(String imageUrl, String filetype) {
    this.imageUrl = imageUrl;
    this.filetype = filetype;
  }

  void _handleDoubleTapDown(TapDownDetails details) {
    _doubleTapDetails = details;
  }

// double tap zoom the image------
  void _handleDoubleTap() {
    if (_transformationController.value != Matrix4.identity()) {
      _transformationController.value = Matrix4.identity();
    } else {
      final position = _doubleTapDetails.localPosition;
      // For a 3x zoom
      _transformationController.value = Matrix4.identity()
        ..translate(-position.dx * 2, -position.dy * 2)
        ..scale(3.0);
      // Fox a 2x zoom
      // ..translate(-position.dx, -position.dy)
      // ..scale(2.0);

    }
  }

  @override
  _AttachmentImageFullViewState createState() =>
      _AttachmentImageFullViewState();
}

class _AttachmentImageFullViewState extends State<AttachmentImageFullView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black,
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: const Text('Image details'),
          centerTitle: true,
        ),
        body: ImageFullviewWidget());
  }

  // ignore: non_constant_identifier_names
  Widget ImageFullviewWidget() {
    return Container(
        height: double.infinity,
        width: double.infinity,
        color: Colors.black,
        child: GestureDetector(
          onDoubleTapDown: widget._handleDoubleTapDown,
          onDoubleTap: widget._handleDoubleTap,
          child: Center(
            child: InteractiveViewer(
              transformationController: widget._transformationController,
              panEnabled: false, // Set it to false to prevent panning.
              boundaryMargin: EdgeInsets.all(80),
              minScale: 0.5,
              maxScale: 4,
              child: Image(
                image: widget.filetype == "FILE"
                    ? File(widget.imageUrl)
                    : NetworkImage(widget.imageUrl),
                height: double.infinity,
                width: double.infinity,
              ),
            ),
          ),
        ));
  }
}
