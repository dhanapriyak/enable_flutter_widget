import 'package:flutter/cupertino.dart';

// media attachment class
class Attachment {
  String mediaType;
  String mediaUrl;
  String mediaThumb;
  String fileType;

  Attachment({
    @required this.mediaUrl,
    @required this.mediaType,
    @required this.fileType,
    this.mediaThumb,
  });
}
