import 'dart:io';

import 'package:enable_flutter_widgets/ChatBubble/model/attachment.dart';
import 'package:enable_flutter_widgets/ChatBubble/widgets/attachment_image_fullview.dart';
import 'package:enable_flutter_widgets/ChatBubble/widgets/attachment_video_fullview.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class EnableChatBubble extends StatelessWidget {
  String s = "asset";
  //---------Color setting properties---------
  MaterialColor serderborderColor,
      receiverborderColor,
      senderBGColor,
      receiverBGColor,
      senderNameFontColor,
      receiverNameFontColor,
      dateFontColor,
      messageFontColor,
      divColor,
      senderProfilePicColor,
      receiverProfilePicColor,
      attachmentBorderColor;
  //---------Content setting properties---------

  String message, name, date, profilePic;
  //---------size related setting properties---------
  List<Attachment> attachment = [];
  double nameFontSize,
      dateFontSize,
      msgFontSize,
      divHeight,
      profileImgSize,
      cardRoundRadius;
  //---------visible show/hide prperties---------

  bool sender = true, showProfileImg, showAlertImg, newMsg;

  EnableChatBubble({
    Key key,
    this.serderborderColor,
    this.receiverborderColor,
    this.attachmentBorderColor,
    this.receiverProfilePicColor,
    this.senderProfilePicColor,
    @required this.date,
    this.attachment,
    this.messageFontColor,
    this.cardRoundRadius,
    this.profilePic,
    this.dateFontColor,
    this.senderBGColor,
    this.receiverBGColor,
    this.divColor,
    this.showProfileImg,
    this.divHeight,
    this.senderNameFontColor,
    this.receiverNameFontColor,
    this.showAlertImg,
    this.profileImgSize,
    this.nameFontSize,
    this.dateFontSize,
    this.msgFontSize,
    @required this.name,
    @required this.sender,
    this.message,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: sender == true
            ? EdgeInsets.only(left: 50)
            : EdgeInsets.only(right: 50),
        child: Wrap(
          children: [
            //--------chat bubble card-------
            Card(
                color: sender
                    ? (senderBGColor != null
                        ? senderBGColor
                        : Color(0xffE6F7F2))
                    : (receiverBGColor != null
                        ? receiverBGColor
                        : Colors.white),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        cardRoundRadius != null ? cardRoundRadius : 5.0),
                    side: BorderSide(
                        color: sender
                            ? (serderborderColor != null
                                ? serderborderColor
                                : Colors.black12)
                            : (receiverborderColor != null
                                ? receiverborderColor
                                : Colors.black12),
                        width: 1)),
                elevation: 1,
                child: Container(
                  alignment: Alignment.centerLeft,
                  padding: EdgeInsets.all(10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              //--------alert image view-------------
                              Visibility(
                                  visible: showAlertImg != null
                                      ? showAlertImg
                                      : false,
                                  child: Row(
                                    children: [
                                      Image.asset(
                                        'assets/images/flags.png',
                                        height: profileImgSize != null
                                            ? profileImgSize
                                            : 18,
                                        width: profileImgSize != null
                                            ? profileImgSize
                                            : 18,
                                      ),
                                      SizedBox(
                                        width: 5,
                                      ),
                                    ],
                                  )),
                              //--------profile image view-------------
                              Visibility(
                                  visible: showProfileImg != null
                                      ? showProfileImg
                                      : true,
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(20.0),
                                    child: Image.asset(
                                        sender
                                            ? (profilePic != null
                                                ? profilePic
                                                : 'assets/images/doctor_3.jpg')
                                            : (profilePic != null
                                                ? profilePic
                                                : 'assets/images/patient.jpg'),
                                        height: profileImgSize != null
                                            ? profileImgSize
                                            : 35,
                                        width: profileImgSize != null
                                            ? profileImgSize
                                            : 35,
                                        fit: BoxFit.cover),
                                  )),
                              SizedBox(width: 5),
                              //--------Name Widget-------------
                              Text(
                                name,
                                style: TextStyle(
                                    color: (sender
                                        ? (senderNameFontColor != null
                                            ? senderNameFontColor
                                            : Color(0xff565662))
                                        : (receiverNameFontColor != null
                                            ? receiverNameFontColor
                                            : Color(0xff0F7BBD))),
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'OpenSans',
                                    fontSize: nameFontSize != null
                                        ? nameFontSize
                                        : 14),
                              )
                            ],
                          ),
                          //--------Date widget-------------
                          Text(
                            date,
                            style: TextStyle(
                                color: (dateFontColor != null
                                    ? dateFontColor
                                    : Color(0x99565662)),
                                fontFamily: 'OpenSans',
                                fontSize:
                                    dateFontSize != null ? dateFontSize : 12),
                          )
                        ],
                      ),
                      if (message.isNotEmpty)
                        SizedBox(
                          height: 15,
                        ),
                      if (message.isNotEmpty)
                        //--------message  Widget-------------
                        Text(message,
                            style: TextStyle(
                                color: (messageFontColor != null
                                    ? messageFontColor
                                    : Color(0xff3C3C43)),
                                fontFamily: 'OpenSans',
                                fontSize:
                                    msgFontSize != null ? msgFontSize : 16)),
                      SizedBox(
                        height: 10,
                      ),
                      //--------divider b/w message and attachment-------------
                      if (attachment != null && attachment.length > 0)
                        Container(
                          height: 1,
                          color:
                              (divColor != null ? divColor : Color(0x1A000000)),
                        ),
                      if (attachment != null && attachment.length > 0)
                        SizedBox(height: 10),

                      if (attachment != null && attachment.length > 0)
                        SizedBox(
                            height: 120,
                            child: ListView.separated(
                              shrinkWrap: true,
                              itemCount: attachment.length,
                              scrollDirection: Axis.horizontal,
                              // ignore: missing_return
                              itemBuilder: (BuildContext context, int index) {
                                if (attachment[index].mediaType == "IMAGE") {
                                  // print("url" + attachment[index].mediaUrl);
                                  return imageAttachmentWidget(
                                      context, attachment[index]);
                                } else if (attachment[index].mediaType ==
                                    "VIDEO") {
                                  return videoWidget(
                                      context, attachment[index]);
                                }
                              },
                              separatorBuilder: (context, index) {
                                return Container(
                                  width: 8,
                                );
                              },
                            )),
                      //--------Image attachment widget-------------

                      // if (mediadType == "IMAGE")
                      //   imageAttachmentWidget(context)
                      // //--------Video attachment widget-------------

                      // else if (mediadType == "VIDEO")
                      //   videoWidget(context)
                    ],
                  ),
                ))
          ],
        ));
  }

//--------Image  attachment widget-------------

  Widget imageAttachmentWidget(BuildContext context, Attachment attachment) {
    return Container(
        padding: const EdgeInsets.all(2.0),
        //--------Rounded border to the attachment-------------
        decoration: BoxDecoration(
            border: Border.all(
              width: 1.0,
              color: (attachmentBorderColor != null
                  ? attachmentBorderColor
                  : Color(0xffE8E8E8)),
            ),
            borderRadius: BorderRadius.all(
                Radius.circular(5.0) //                 <--- border radius here
                )),
        child: GestureDetector(
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => AttachmentImageFullView(
                    attachment.mediaUrl, attachment.fileType)));
          },
//--------attachment image-------------

          child: Image(
            image: attachment.fileType == "NETWORK"
                ? NetworkImage(attachment.mediaUrl)
                : FileImage(File(attachment.mediaUrl)),
            height: 100,
            width: 100,
            fit: BoxFit.cover,
          ),
        ));
  }

//--------video  attachment widget-------------
  Widget videoWidget(BuildContext context, Attachment attachment) {
    return Container(
        padding: const EdgeInsets.all(2.0),
        //--------Rounded border to the attachment-------------

        decoration: BoxDecoration(
            border: Border.all(
              width: 1.0,
              color: (attachmentBorderColor != null
                  ? attachmentBorderColor
                  : Color(0xffE8E8E8)),
            ),
            borderRadius: BorderRadius.all(
                Radius.circular(5.0) //                 <--- border radius here
                )),
        //--------added hero for animation. if we click the video open full view with animation-------------

        child: GestureDetector(
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => AttachmentVideoFullview(
                      attachment.mediaUrl, attachment.fileType)));
            },
            //----- show the thumbnail image of the video--------
            child: Container(
                height: 100,
                width: 100,
                child: Stack(
                  children: [
                    Image(
                      image: attachment.fileType == "NETWORK"
                          ? NetworkImage(attachment.mediaThumb)
                          : FileImage(File(attachment.mediaThumb)),
                      height: 100,
                      width: 100,
                      fit: BoxFit.cover,
                    ),
                    //-----video icon widget---
                    Align(
                        alignment: Alignment.center,
                        child: Icon(Icons.play_circle_fill,
                            color: Colors.white, size: 35))
                  ],
                ))));
  }
}
