import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class ShowSelectedImage extends StatefulWidget {
  String _imageFile;
  ShowSelectedImage(String _imageFile) {
    this._imageFile = _imageFile;
  }
  @override
  _ShowSelectedImageState createState() => _ShowSelectedImageState();
}

class _ShowSelectedImageState extends State<ShowSelectedImage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Selected Image"),
          centerTitle: true,
        ),
        body: Container(
          color: Colors.black,
          child: Column(
            children: [
              SizedBox(
                height: 20,
              ),
              Expanded(child: Center(child: _buildImage())),
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  IconButton(
                    icon: Icon(
                      Icons.close,
                      color: Colors.white,
                      size: 40,
                    ),
                    onPressed: () {
                      Navigator.pop(context, false);
                    },
                  ),
                  IconButton(
                      icon: Icon(
                        Icons.check,
                        color: Colors.white,
                        size: 40,
                      ),
                      onPressed: () {
                        Navigator.pop(context, true);
                      })
                ],
              ),
              SizedBox(
                height: 20,
              )
            ],
          ),
        ));
  }

  Widget _buildImage() {
    if (widget._imageFile != null) {
      return Image.file(File(widget._imageFile));
    } else {
      return Text('Take an image to start', style: TextStyle(fontSize: 18.0));
    }
  }
}
