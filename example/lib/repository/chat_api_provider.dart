import 'dart:io';

import 'package:enable_flutter_widgets/ChatBubble/model/attachment.dart';
import 'package:enable_flutter_widgets/ChatBubble/screens/enable_chat_bubble.dart';
import 'package:example/screen/chat_screen.dart';

import 'package:flutter/material.dart';

class ChatApiProvider {
  static List<EnableChatBubble> chatList = [];

  File userImg;

  Future<List<EnableChatBubble>> getChatMessages() async {
    List<Attachment> singleAttachment = [];
    singleAttachment.add(new Attachment(
        mediaUrl:
            'https://46m07ujaoaf39u3i2v16spny-wpengine.netdna-ssl.com/wp-content/uploads/2018/01/what-is-full-body-scan-unique-.jpg',
        fileType: "NETWORK",
        mediaType: "IMAGE"));
    List<Attachment> singleVideoAttachment = [];
    singleVideoAttachment.add(new Attachment(
        mediaUrl:
            ' https://cdn.videvo.net/videvo_files/video/free/2020-04/small_watermarked/200420_Business_02_4k_022_preview.webm',
        mediaType: "VIDEO",
        mediaThumb:
            "https://lymphoma-action.org.uk/sites/default/files/inline-images/X-ray-resized.png",
        fileType: "NETWORK"));

    List<Attachment> multipleAttachment = [];
    multipleAttachment.add(Attachment(
        mediaUrl:
            'https://46m07ujaoaf39u3i2v16spny-wpengine.netdna-ssl.com/wp-content/uploads/2018/01/what-is-full-body-scan-unique-.jpg',
        fileType: "NETWORK",
        mediaType: "IMAGE"));
    multipleAttachment.add(Attachment(
        mediaUrl:
            'https://image.slidesharecdn.com/accreditationdocumentsforhospitalbynabhconsultant-171118122454/95/preentry-level-for-hospital-nabh-documentation-kit-1-638.jpg?cb=1511181255',
        fileType: "NETWORK",
        mediaType: "IMAGE"));
    List<Attachment> multipleVideoAttachment = [];
    multipleVideoAttachment.add(Attachment(
        mediaUrl:
            'https://cdn.videvo.net/videvo_files/video/free/2020-04/small_watermarked/200420_Business_02_4k_022_preview.webm',
        mediaType: "VIDEO",
        mediaThumb:
            "https://image.slidesharecdn.com/accreditationdocumentsforhospitalbynabhconsultant-171118122454/95/preentry-level-for-hospital-nabh-documentation-kit-1-638.jpg?cb=1511181255",
        fileType: "NETWORK"));
    multipleVideoAttachment.add(Attachment(
        mediaUrl:
            'https://cdn.videvo.net/videvo_files/video/free/2020-02/small_watermarked/200129_01_Medical_4k_097_preview.webm',
        mediaType: "VIDEO",
        fileType: "NETWORK",
        mediaThumb:
            "https://images-na.ssl-images-amazon.com/images/I/71eq9Xhwb1L.png"));
    multipleVideoAttachment.add(Attachment(
        mediaUrl:
            'https://cdn.videvo.net/videvo_files/video/free/2020-04/small_watermarked/200420_Business_02_4k_022_preview.webm',
        mediaType: "VIDEO",
        fileType: "NETWORK",
        mediaThumb:
            "https://lymphoma-action.org.uk/sites/default/files/inline-images/X-ray-resized.png"));
    List<Attachment> multipleVideoandImageAttachment = [];
    multipleVideoandImageAttachment.add(Attachment(
        mediaUrl:
            'https://cdn.videvo.net/videvo_files/video/free/2020-03/small_watermarked/200129_03_Medical_4k_016_preview.webm',
        mediaType: "VIDEO",
        fileType: "NETWORK",
        mediaThumb:
            "https://images-na.ssl-images-amazon.com/images/I/71eq9Xhwb1L.png"));

    multipleVideoandImageAttachment.add(Attachment(
        mediaUrl:
            'https://cdn.videvo.net/videvo_files/video/free/2020-04/small_watermarked/200420_Business_02_4k_022_preview.webm',
        mediaType: "VIDEO",
        fileType: "NETWORK",
        mediaThumb:
            "https://46m07ujaoaf39u3i2v16spny-wpengine.netdna-ssl.com/wp-content/uploads/2018/01/what-is-full-body-scan-unique-.jpg"));
    multipleVideoandImageAttachment.add(Attachment(
        mediaUrl:
            'https://image.slidesharecdn.com/accreditationdocumentsforhospitalbynabhconsultant-171118122454/95/preentry-level-for-hospital-nabh-documentation-kit-1-638.jpg?cb=1511181255',
        fileType: "NETWORK",
        mediaType: "IMAGE"));

    multipleVideoandImageAttachment.add(Attachment(
        mediaUrl:
            'https://46m07ujaoaf39u3i2v16spny-wpengine.netdna-ssl.com/wp-content/uploads/2018/01/what-is-full-body-scan-unique-.jpg',
        fileType: "NETWORK",
        mediaType: "IMAGE"));

    multipleVideoandImageAttachment.add(Attachment(
        mediaUrl:
            'https://cdn.videvo.net/videvo_files/video/free/2020-04/small_watermarked/200420_Business_02_4k_022_preview.webm',
        mediaType: "VIDEO",
        fileType: "NETWORK",
        mediaThumb:
            "https://46m07ujaoaf39u3i2v16spny-wpengine.netdna-ssl.com/wp-content/uploads/2018/01/what-is-full-body-scan-unique-.jpg"));

    chatList.add(EnableChatBubble(
        key: UniqueKey(),
        message: "Multiple video and image Attchment",
        sender: false,
        attachment: multipleVideoandImageAttachment,
        name: "Dr Brian Miller",
        date: "5:58 PM  "));
    chatList.add(EnableChatBubble(
        key: UniqueKey(),
        message: "Multiple video Attchment",
        sender: true,
        attachment: multipleVideoAttachment,
        name: "Dr Brian Miller",
        date: "5:58 PM  "));
    chatList.add(EnableChatBubble(
        key: UniqueKey(),
        senderProfilePicColor: Colors.red,
        message: "Single video  attachment",
        sender: false,
        showAlertImg: false,
        attachment: singleVideoAttachment,
        name: "Sarah Brown",
        date: "8:30 AM   Today"));

    chatList.add(EnableChatBubble(
        key: UniqueKey(),
        message: "Multiple Image Attchment",
        sender: true,
        attachment: multipleAttachment,
        name: "Dr Brian Miller",
        date: "5:58 PM  "));
    chatList.add(EnableChatBubble(
        key: UniqueKey(),
        senderProfilePicColor: Colors.red,
        message: "Single Image attachment",
        sender: false,
        showAlertImg: false,
        attachment: singleAttachment,
        name: "Sarah Brown",
        date: "8:30 AM   Today"));
    chatList.add(EnableChatBubble(
        key: UniqueKey(),
        senderProfilePicColor: Colors.red,
        message: "Disabled Profile Picture.",
        sender: false,
        showAlertImg: true,
        showProfileImg: false,
        name: "Sarah Brown",
        date: "8:30 AM   Today"));

    chatList.add(EnableChatBubble(
        key: UniqueKey(),
        message: "Disabled Profile Picture.",
        sender: true,
        showProfileImg: false,
        name: "Dr Brian Miller",
        date: "5:58 PM  "));
    chatList.add(EnableChatBubble(
        key: UniqueKey(),
        senderProfilePicColor: Colors.red,
        message:
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit.  Cras ac ultricies enim. Nunc fringilla sit amet ex sed convallis. Duis bibendum venenatis https://altais.com/ tincidunt.",
        sender: false,
        showAlertImg: false,
        name: "Sarah Brown",
        date: "8:30 AM   Today"));

    chatList.add(EnableChatBubble(
        key: UniqueKey(),
        message:
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit.  Cras ac ultricies enim. Nunc fringilla sit amet ex sed convallis. Duis bibendum venenatis https://altais.com/ tincidunt.",
        sender: true,
        name: "Dr Brian Miller",
        date: "5:58 PM  "));

    return chatList;
  }

  Future<List<EnableChatBubble>> addChatMessages(
      List<EnableChatBubble> chatlist, EnableChatBubble newChat) async {
    List<EnableChatBubble> updatedChatMsg = [];
    EnableChatBubble receverSide = EnableChatBubble(
      name: newChat.name,
      message: newChat.message,
      sender: false,
      date: newChat.date,
      attachment: newChat.attachment,
    );

    updatedChatMsg.add(receverSide);

    updatedChatMsg.add(newChat);

    updatedChatMsg.addAll(chatList);

    chatList = updatedChatMsg;

    return updatedChatMsg;
  }
}
