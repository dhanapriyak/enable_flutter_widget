import 'package:enable_flutter_widgets/ChatBubble/screens/enable_chat_bubble.dart';
import 'package:get_it/get_it.dart';

import 'chat_api_provider.dart';

class Repository {
  final chatApiProvider = GetIt.I.get<ChatApiProvider>();

  Future<List<EnableChatBubble>> getChatMessages() =>
      chatApiProvider.getChatMessages();
  Future<List<EnableChatBubble>> addChatMsg(
          List<EnableChatBubble> chatlist, EnableChatBubble newChat) =>
      chatApiProvider.addChatMessages(chatlist, newChat);
}
