import 'dart:io';

import 'package:enable_flutter_widgets/ChatBubble/model/attachment.dart';
import 'package:enable_flutter_widgets/ChatBubble/screens/enable_chat_bubble.dart';
import 'package:example/bloc/chat/chat_cubit.dart';
import 'package:example/widget/camera_screen.dart';
import 'package:example/widget/show_image_view.dart';
import 'package:example/widget/video_fullview_screen.dart';

import 'package:file_picker/file_picker.dart';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:image_picker/image_picker.dart';
import 'package:thumbnails/thumbnails.dart';

// ignore: must_be_immutable
class ChatScreen extends StatefulWidget {
  bool visiblecht = false;
  bool isImageOk = false;

  List<Attachment> attachments = [];

  String imgPath;
  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  int onclicked = 0;
  @override
  void initState() {
    GetIt.I.get<GetchatCubit>().getChatMessage();

    super.initState();
  }

  TextEditingController _chatMsgController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: BlocBuilder<GetchatCubit, GetchatState>(
      cubit: GetIt.I.get<GetchatCubit>(),
      builder: (context, state) {
        print("*****State**");
        print(state);
        switch (state.runtimeType) {
          case GetChatMessageInitial:
            return chatEmptyWidget();
            break;
          case GetChatMessageInitiated:
            return chatEmptyWidget();
            break;
          case GetChatMessageLoaded:
            return chatWidget(
                (state as GetChatMessageLoaded).chatMessageResponse);
            break;
          case AddChatSuccess:
            return chatWidget((state as AddChatSuccess).chatMessageResponse);

            break;
          case GetChatMessageFailed:
            return chatEmptyWidget();
            break;
          case GetChatMessageInitial:
          default:
            return chatEmptyWidget();
        }
      },
    ));
  }

  Widget chatEmptyWidget() {
    List<EnableChatBubble> chatList = [];
    return Column(
      children: <Widget>[
        Expanded(child: Container()),
        _buildMessageComposer(chatList),
      ],
    );
  }

  Widget chatWidget(List<EnableChatBubble> chatList) {
    return Column(
      children: <Widget>[
        Expanded(
          child: Container(
              color: Color(0xffE5E5E5),
              padding: EdgeInsets.all(10),
              //-------chat list view--------
              child: ListView.separated(
                padding: EdgeInsets.only(top: 15.0),
                itemCount: chatList.length,
                reverse: true,
                shrinkWrap: true,
                itemBuilder: (BuildContext context, int index) {
                  if (chatList[index].attachment != null &&
                      chatList[index].attachment.length > 0) {}
                  return chatList[index];
                },
                separatorBuilder: (context, index) {
                  return Container(
                    height: 20,
                  );
                },
              )),
        ),
        // ---------- chat composer view-----------
        _buildMessageComposer(chatList),
      ],
    );
  }

  _buildMessageComposer(List<EnableChatBubble> chatList) {
    return Container(
        color: Colors.white,
        child: Column(
          children: [
            //-----------ask attachmet widget from gallery or camera-------
            Visibility(
              visible: widget.visiblecht,
              child: Container(
                  color: Color(0xffF5F1F0),
                  padding: EdgeInsets.all(15),
                  height: 120,
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Column(
                          children: [
                            ClipOval(
                              child: Material(
                                color: Colors.pink, // button color
                                child: InkWell(
                                  splashColor: Colors.white12, // inkwell color
                                  child: SizedBox(
                                      width: 56,
                                      height: 56,
                                      child: Icon(Icons.photo,
                                          color: Colors.white)),
                                  onTap: () {
                                    setState(() {
                                      if (widget.visiblecht == true)
                                        widget.visiblecht = false;
                                      else
                                        widget.visiblecht = true;
                                    });
                                    _openFileExplorer();
                                    // captureImage(ImageSource.gallery);
                                  },
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              "Gallery",
                            )
                          ],
                        ),
                        SizedBox(
                          width: 50,
                          height: 20,
                        ),
                        Column(
                          children: [
                            ClipOval(
                              child: Material(
                                color: Colors.purple, // button color
                                child: InkWell(
                                  splashColor: Colors.white12, // inkwell color
                                  child: SizedBox(
                                      width: 56,
                                      height: 56,
                                      child: Icon(Icons.photo,
                                          color: Colors.white)),
                                  onTap: () {
                                    setState(() {
                                      if (widget.visiblecht == true)
                                        widget.visiblecht = false;
                                      else
                                        widget.visiblecht = true;
                                    });
                                    Future result = Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) => CameraScreen(),
                                        ));

                                    result.then((value) => setState(() async {
                                          List<String> details = value;
                                          widget.imgPath = details[0];

                                          print("file pathhhhhhh" +
                                              widget.imgPath);
                                          if (widget.imgPath.contains("jpg") ||
                                              widget.imgPath.contains("png") ||
                                              widget.imgPath.contains("jpeg")) {
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        ShowSelectedImage(
                                                            widget.imgPath)));
                                            setState(() {
                                              widget.imgPath = details[0];
                                              widget.attachments.add(
                                                new Attachment(
                                                  mediaUrl: details[0],
                                                  fileType: "FILE",
                                                  mediaType: "IMAGE",
                                                ),
                                              );

                                              widget.isImageOk = true;
                                            });
                                          } else {
                                            // String thumimg = await getThumImage(
                                            //     widget.imgPath);

                                            // setState(() {
                                            //   widget.thumpPath = thumimg;
                                            // });

                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        VideoFullviewScreen(
                                                            details[0])));
                                            setState(() {
                                              widget.attachments.add(
                                                new Attachment(
                                                    mediaUrl: details[0],
                                                    mediaType: "VIDEO",
                                                    fileType: "FILE",
                                                    mediaThumb: details[1]),
                                              );

                                              widget.imgPath = details[1];
                                              // widget.imgPath = widget.thumpPath;
                                              widget.isImageOk = true;
                                            });
                                          }
                                        }));

                                    // widget.imgPath = result.toString();
                                    // final resultt = Navigator.push(
                                    //     context,
                                    //     MaterialPageRoute(
                                    //         builder: (context) =>
                                    //             ShowSelectedImage(
                                    //                 widget.imgPath)));
                                  },
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              "Camera",
                            )
                          ],
                        ),
                      ])),
            ),
            if (widget.isImageOk == true)
              // ----------- show attached image for user reference ----------
              Visibility(
                  visible: widget.isImageOk,
                  child: Container(
                      padding: EdgeInsets.all(5),
                      height: 120,
                      child: ListView.separated(
                        reverse: true,
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        padding: EdgeInsets.only(top: 15.0),
                        itemCount: widget.attachments.length,
                        // ignore: missing_return
                        itemBuilder: (BuildContext context, int index) {
                          return widget.attachments[index].mediaType == "IMAGE"
                              ? Container(
                                  height: 100,
                                  width: 100,
                                  decoration: BoxDecoration(
                                      border: Border.all(
                                          width: 1.0, color: Color(0xffE8E8E8)),
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(
                                              5.0) //                 <--- border radius here
                                          )),
                                  child: Image(
                                    image: widget.attachments[index].fileType ==
                                            "FILE"
                                        ? FileImage(File(
                                            widget.attachments[index].mediaUrl))
                                        : widget.attachments[index].mediaUrl,
                                    fit: BoxFit.fill,
                                  ))
                              : Container(
                                  height: 100,
                                  width: 100,
                                  decoration: BoxDecoration(
                                      border: Border.all(
                                          width: 1.0, color: Color(0xffE8E8E8)),
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(
                                              5.0) //                 <--- border radius here
                                          )),
                                  child: Stack(
                                    children: [
                                      Image.file(
                                        File(widget
                                            .attachments[index].mediaThumb),
                                        height: 100,
                                        width: 100,
                                        fit: BoxFit.fill,
                                      ),
                                      //-----video icon widget---
                                      Align(
                                          alignment: Alignment.center,
                                          child: Icon(Icons.play_circle_fill,
                                              color: Colors.white, size: 35))
                                    ],
                                  ));
                        },
                        separatorBuilder: (context, index) {
                          return Container(
                            width: 10,
                          );
                        },
                      ))),
            Container(
              height: 1,
              color: Colors.black45,
            ),
            //-----------chat widget----------
            Container(
              padding: EdgeInsets.only(top: 10, bottom: 10),
              child: Row(
                children: <Widget>[
                  //---------attachment icon--------
                  IconButton(
                      icon: Icon(Icons.attach_file),
                      iconSize: 30.0,
                      color: Theme.of(context).primaryColor,
                      onPressed: () {
                        if (onclicked > 0) {
                          onclicked = 0;
                          widget.attachments.clear();
                        }
                        setState(() {
                          if (widget.visiblecht == true)
                            widget.visiblecht = false;
                          else
                            widget.visiblecht = true;
                        });
                      }),
                  //--------chat textfield--------
                  Expanded(
                    child: TextField(
                      key: Key("textfield"),
                      textAlignVertical: TextAlignVertical.center,
                      keyboardType: TextInputType.multiline,
                      maxLength: 1600,
                      controller: _chatMsgController,
                      maxLines: 5,
                      minLines: 1,
                      decoration: InputDecoration(
                        hintText: 'Send a message...',
                        contentPadding: EdgeInsets.all(7.0),
                        border: new OutlineInputBorder(
                            borderSide: new BorderSide(color: Colors.teal)),
                      ),
                      textCapitalization: TextCapitalization.sentences,
                    ),
                  ),
                  //--------send button-------
                  IconButton(
                    key: Key("sendButton"),
                    icon: Icon(Icons.send),
                    iconSize: 30.0,
                    color: Theme.of(context).primaryColor,
                    onPressed: () {
                      setState(() {
                        widget.isImageOk = false;
                      });
                      onclicked = 1;

                      //----------form the new chat item and call add chat method---------
                      GetIt.I.get<GetchatCubit>().addChatMessage(
                          chatList,
                          EnableChatBubble(
                              key: UniqueKey(),
                              message: _chatMsgController.text,
                              attachment: widget.attachments,
                              sender: true,
                              name: "Laura Johnson",
                              date: "05 Jan 21, 12:05"));
                      _chatMsgController.text = "";
                      widget.attachments = [];
                    },
                  ),
                ],
              ),
            )
          ],
        ));
  }

//--------get attachment from camera---------
  Future<void> captureImage(ImageSource imageSource) async {
    try {
      final imageFile = await ImagePicker.pickImage(source: imageSource);

      widget.imgPath = imageFile.path;
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => ShowSelectedImage(widget.imgPath)));

      setState(() {
        widget.attachments.add(new Attachment(
            mediaUrl: imageFile.path, fileType: "FILE", mediaType: "IMAGE"));
        widget.isImageOk = true;
      });
    } catch (e) {
      print(e);
    }
  }

//-------get attachment from gallery-------
  void _openFileExplorer() async {
    FilePickerResult result =
        await FilePicker.platform.pickFiles(allowMultiple: true);

    if (result != null) {
      List<File> files = result.paths.map((path) => File(path)).toList();
      widget.imgPath = files[0].path;

      if (files[0].path.contains(".jpg") ||
          files[0].path.contains(".png") ||
          files[0].path.contains(".jpeg")) {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ShowSelectedImage(files[0].path)));

        setState(() {
          widget.attachments.add(new Attachment(
              mediaUrl: files[0].path, fileType: "FILE", mediaType: "IMAGE"));

          widget.isImageOk = true;
        });
      } else {
        String thumb = await Thumbnails.getThumbnail(
            videoFile: files[0].path, imageType: ThumbFormat.PNG, quality: 30);
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => VideoFullviewScreen(files[0].path)));
        setState(() {
          widget.attachments.add(new Attachment(
              mediaUrl: files[0].path,
              mediaType: "VIDEO",
              mediaThumb: thumb,
              fileType: "FILE"));

          widget.isImageOk = true;
        });
      }
    }
  }

// getting video thumb
  Future<String> getThumImage(String path) async {
    return await Thumbnails.getThumbnail(
        videoFile: path, imageType: ThumbFormat.PNG, quality: 30);
  }
}
