import 'package:example/repository/chat_api_provider.dart';
import 'package:example/repository/repository.dart';
import 'package:example/screen/chat_screen.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

import 'bloc/chat/chat_cubit.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    _setUp();
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: const Text('Chat'),
          centerTitle: true,
        ),
        body: ChatScreen(),
      ),
    );
  }
}

void _setUp() {
  print("Inside setup");
//-----------setup the repository and cubits--------
  GetIt.I.registerSingleton<ChatApiProvider>(ChatApiProvider());
  GetIt.I.registerSingleton<Repository>(Repository());
  GetIt.I.registerSingleton<GetchatCubit>(GetchatCubit());
}
