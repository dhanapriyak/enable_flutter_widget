part of 'chat_cubit.dart';

@immutable
abstract class GetchatState {}

class GetchatInitial extends GetchatState {}

class GetChatMessageInitial extends GetchatState {}

class GetChatMessageInitiated extends GetchatState {}

class GetChatMessageLoaded extends GetchatState {
  final List<EnableChatBubble> chatMessageResponse;
  GetChatMessageLoaded(this.chatMessageResponse);
}

class AddChatSuccess extends GetchatState {
  final List<EnableChatBubble> chatMessageResponse;
  AddChatSuccess(this.chatMessageResponse);
}

class GetChatMessageFailed extends GetchatState {
  final String message;
  GetChatMessageFailed(this.message);
}
