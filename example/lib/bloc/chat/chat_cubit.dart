import 'package:bloc/bloc.dart';
import 'package:enable_flutter_widgets/ChatBubble/screens/enable_chat_bubble.dart';
import 'package:example/repository/repository.dart';
import 'package:get_it/get_it.dart';
import 'package:meta/meta.dart';

part 'chat_state.dart';

class GetchatCubit extends Cubit<GetchatState> {
  Repository _repository;
  GetchatCubit() : super(GetchatInitial()) {
    _repository = GetIt.I.get<Repository>();
  }
  // ignore: missing_return
  Future<List<EnableChatBubble>> getChatMessage() async {
    emit(GetChatMessageInitiated());
    await _repository.getChatMessages().then((chatMessageresponse) {
      emit(GetChatMessageLoaded(chatMessageresponse));
    }).catchError((Object e) {
      print("error" + e.toString());
      emit(GetChatMessageFailed("Some error -- " + e.toString()));
    });
  }

  Future<List<EnableChatBubble>> addChatMessage(
      List<EnableChatBubble> chatList, EnableChatBubble newChat) async {
    emit(GetChatMessageInitiated());
    await _repository.addChatMsg(chatList, newChat).then((chatMessageresponse) {
      emit(AddChatSuccess(chatMessageresponse));
    }).catchError((Object e) {
      print("error" + e.toString());
      emit(GetChatMessageFailed("Some error -- " + e.toString()));
    });
  }
}
